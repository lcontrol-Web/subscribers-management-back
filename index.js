const express = require('express');
const morgan = require('morgan');
const compression = require('compression');
const cors = require('cors');
const bodyParser = require('body-parser');
const app = express();
const settings = require('./settings.json');
const log = require('./global-functions/console');


var httpProtocol = null;
var io = null;

/* ************** using HTTP ************** */
if (!settings.https) {
  httpProtocol = require('http').Server(app);
}
/********************************************/

/* ************** using HTTPS ************** */
else {
  const fs = require('fs');

  const options = {
    key: fs.readFileSync('sniper-key.pem'),
    cert: fs.readFileSync('sniper-crt.pem')
  }

  httpProtocol = require('https').Server(options, app);
}
/*********************************************/





// allow access from other services
app.use(cors());
app.use((req, res, next) => {
    res.append('Access-Control-Allow-Headers', 'Content-Type');
    //res.setHeader('Access-Control-Expose-Headers', 'x-auth-token');
    next();
})

// support parsing of application/json type post data
app.use(bodyParser.json({ limit: "50mb" }));

// support parsing of application/x-www-form-urlencoded post data
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));

// decrease the size of the response body and hence increase the speed of a web app
app.use(compression());

// HTTP request logger
app.use(morgan('tiny'));

require('./logging')();

// set the restful API routes
require('./routes/routes')(app);

// connect to database
require('./db');


httpProtocol.listen(3333, () => {
    log.info('Listening on port 3333');
});

function setTerminalTitle(title) {
    process.stdout.write(
        String.fromCharCode(27) + "]0;" + title + String.fromCharCode(7)
    );
}

setTerminalTitle('Main');