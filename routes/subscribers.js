const express = require('express');
const db = require('../db');
const log = require('../global-functions/console');
const router = express.Router();
const crypto = require('crypto');
const dbLogs = require('../global-functions/db-logs');

const SCHEMA = 'subscription_management';

router.post('/get-subscribers', async (req, res) => {
    try {
        var query = `
            SELECT * FROM ${SCHEMA}.subscribers; 
        
            SELECT * FROM ${SCHEMA}.purchases ORDER BY ${SCHEMA}.purchases.experationDate DESC;

            SELECT * FROM ${SCHEMA}.serial_numbers ORDER BY ${SCHEMA}.serial_numbers.activationDate DESC;
        `;

        var response = await db.query(query);

        let subscribers = response[0];
        let purchases = response[1];
        let serialNumbers = response[2];

        let today = new Date();

        for (let subscriber of subscribers) {
            subscriber.status = 'Not Purchased';
            for (let purchase of purchases) {
                if (purchase.subscriberId == subscriber.subscriberId) {
                    subscriber.lastExperationDate = purchase.experationDate;

                    // check status
                    if (new Date(subscriber.lastExperationDate).getTime() < today.getTime())
                        subscriber.status = 'Expired';

                    else {
                        let preMonth = new Date(subscriber.lastExperationDate);
                        preMonth.setMonth(preMonth.getMonth() - 1);

                        if (preMonth.getTime() < today.getTime())
                            subscriber.status = 'Experation Alert';

                        else
                            subscriber.status = 'Active';
                    }
                    break;
                }
            }

            for (let serialNumberData of serialNumbers) {
                if (serialNumberData.subscriberId == subscriber.subscriberId) {
                    subscriber.lastSerialNumber = serialNumberData.serialNumber;
                    break;
                }
            }
        }

        res.send(subscribers);
    }
    catch (ex) {
        log.error(ex.message);
        res.status(500).send(ex.message);
    }
});


router.post('/add-subscriber', async (req, res) => {
    try {
        var subscriber = req.body;
        var query = `INSERT INTO ${SCHEMA}.subscribers (macAddress, subscriptionDate, email, fullName , phone)
                     VALUES (
                     '${subscriber.macAddress}',
                     '${subscriber.subscriptionDate}',
                     '${subscriber.email}',
                     '${subscriber.fullName}', 
                      ${subscriber.phone ? "'" + subscriber.phone + "'" : 'null'});
                     
                    SELECT * 
                    FROM ${SCHEMA}.subscribers
                    WHERE ${SCHEMA}.subscribers.subscriberId = LAST_INSERT_ID();`;

        var response = await db.query(query);

        res.send(response[1][0]);

        dbLogs.logAction(`Add Subscriber "${subscriber.macAddress}"`, `Subscriber "${subscriber.macAddress}" was added`, subscriber.operatorId);
    }
    catch (ex) {
        log.error(ex.message);
        res.status(500).send(ex.message);
    }
});

router.post('/edit-subscriber', async (req, res) => {
    try {
        var subscriber = req.body;

        // get old data and compare changes
        var query = `SELECT * FROM ${SCHEMA}.subscribers WHERE ${SCHEMA}.subscribers.subscriberId = ${subscriber.subscriberId};`;
        var response = await db.query(query);
        var oldSubscriber = response[0];
        var changes = '';

        for (let field of Object.keys(oldSubscriber)) {
            if (oldSubscriber[field] != subscriber[field] && field !== 'password')
                changes += `${field}: "${oldSubscriber[field]}" => "${subscriber[field]}";`;
        }

        query = `
            UPDATE ${SCHEMA}.subscribers SET
                macAddress = '${subscriber.macAddress}',
                email = '${subscriber.email}',
                fullName = '${subscriber.fullName}', 
                phone = ${subscriber.phone ? "'" + subscriber.phone + "'" : 'null'}
            WHERE ${SCHEMA}.subscribers.subscriberId = ${subscriber.subscriberId};
            
            SELECT subscription.*, lastPurchase.experationDate as 'lastExperationDate' 
            FROM (
                SELECT *
                FROM ${SCHEMA}.subscribers
                WHERE ${SCHEMA}.subscribers.subscriberId = ${subscriber.subscriberId}
            ) as subscription
            LEFT JOIN
            (
                SELECT * 
                FROM ${SCHEMA}.purchases 
                WHERE ${SCHEMA}.purchases.subscriberId = ${subscriber.subscriberId}
                ORDER BY ${SCHEMA}.purchases.experationDate Desc
                LIMIT 1
            ) as lastPurchase
            
            ON lastPurchase.subscriberId = subscription.subscriberId;`;

        response = await db.query(query);

        let item = response[1][0];

        // check status
        let today = new Date();

        if (!item.lastExperationDate)
            item.status = 'Not purchased';

        else if (new Date(item.lastExperationDate).getTime() < today.getTime())
            item.status = 'Expired';

        else {
            let preMonth = new Date(item.lastExperationDate);
            preMonth.setMonth(preMonth.getMonth() - 1);

            if (preMonth.getTime() < today.getTime())
                item.status = 'Experation Alert';

            else
                item.status = 'Active';
        }

        res.send(item);

        // log the changes
        if (changes.length)
            dbLogs.logAction(`Update Subscriber "${subscriber.macAddress}"`, `Subscriber "${oldSubscriber.macAddress}" updated;` + changes, subscriber.operatorId);
    }
    catch (ex) {
        log.error(ex.message);
        res.status(500).send(ex.message);
    }
});

router.post('/delete-subscriber', async (req, res) => {
    try {
        var subscriberId = req.body.subscriberId;

        var query = `SELECT macAddress FROM ${SCHEMA}.subscribers WHERE subscriberId = ${subscriberId};`;

        var response = await db.query(query);
        var macAddress = response[0].macAddress;

        query = `DELETE FROM ${SCHEMA}.subscribers WHERE subscriberId = ${subscriberId};`;

        response = await db.query(query);

        res.send(response);

        // log the action
        dbLogs.logAction(`Delete Subscriber "${macAddress}"`, `Subscriber "${macAddress}" was deleted`, req.body.operatorId);
    }
    catch (ex) {
        log.error(ex.message);
        res.status(500).send(ex.message);
    }
});

router.post('/new-purchase', async (req, res) => {
    try {
        var purchase = req.body;

        var query = `SELECT macAddress FROM ${SCHEMA}.subscribers WHERE subscriberId = ${purchase.subscriberId};`;

        var response = await db.query(query);
        var macAddress = response[0].macAddress;

        query = `INSERT INTO ${SCHEMA}.purchases (subscriberId, purchaseDate, experationDate, cost, currency)
                     VALUES (${purchase.subscriberId},
                     '${purchase.purchaseDate}', 
                     '${purchase.experationDate}', 
                     '${purchase.cost}',
                     '${purchase.currency}');
                     
                    SELECT experationDate 
                    FROM ${SCHEMA}.purchases
                    WHERE ${SCHEMA}.purchases.purchaseId = LAST_INSERT_ID();`;

        response = await db.query(query);

        res.send(response[1][0]);

        // log the action
        dbLogs.logAction(`"${macAddress}", New Purchase`, `Subscriber "${macAddress}" asigned new purchase.;From: ${purchase.purchaseDate};To: ${purchase.experationDate}`, req.body.operatorId);
    }
    catch (ex) {
        log.error(ex.message);
        res.status(500).send(ex.message);
    }
});

router.post('/get-purchases-history', async (req, res) => {
    try {
        var query = `SELECT * 
                     FROM ${SCHEMA}.purchases 
                     WHERE ${SCHEMA}.purchases.subscriberId = ${req.body.subscriberId}
                     ORDER BY ${SCHEMA}.purchases.purchaseDate DESC; `;

        var response = await db.query(query);

        res.send(response);

    }
    catch (ex) {
        log.error(ex.message);
        res.status(500).send(ex.message);
    }
});


router.post('/activate-serial-number', async (req, res) => {
    try {
        var serialNumberData = req.body;

        var query = `SELECT macAddress FROM ${SCHEMA}.subscribers WHERE subscriberId = ${serialNumberData.subscriberId};`;

        var response = await db.query(query);
        var macAddress = response[0].macAddress;

        var query = `INSERT INTO ${SCHEMA}.serial_numbers (serialNumber, activationDate, subscriberId)
                     VALUES ('${serialNumberData.serialNumber}',
                     '${serialNumberData.activationDate}', 
                      ${serialNumberData.subscriberId});
                     
                    SELECT serialNumber 
                    FROM ${SCHEMA}.serial_numbers
                    WHERE ${SCHEMA}.serial_numbers.serialNumberId = LAST_INSERT_ID();`;

        var response = await db.query(query);

        res.send(response[1][0]);

        // log the action
        let d = decodeLicense(serialNumberData.serialNumber);

        dbLogs.logAction(`"${macAddress}", New License`, `Subscriber "${macAddress}" asigned new license.;Activation Date: ${serialNumberData.activationDate};clients: ${d.clients};doors: ${d.doors};cameras: ${d.cameras};zones: ${d.zones};lprs: ${d.lprs};tags: ${d.tags};fire detectors: ${d.fireDetectors};fingerPrints: ${d.fingerPrints};face recognitions: ${d.faceRecognitions};`, req.body.operatorId);
    }
    catch (ex) {
        log.error(ex.message);
        res.status(500).send(ex.message);
    }
});

router.post('/get-serial-numbers-history', async (req, res) => {
    try {
        var query = `SELECT * 
                     FROM ${SCHEMA}.serial_numbers 
                     WHERE ${SCHEMA}.serial_numbers.subscriberId = ${req.body.subscriberId}
                     ORDER BY ${SCHEMA}.serial_numbers.activationDate DESC; `;

        var response = await db.query(query);

        for (let x of response) {
            let decodedData = decodeLicense(x.serialNumber);
            x.decodedLicense = decodedData;
        };

        res.send(response);
    }
    catch (ex) {
        log.error(ex.message);
        res.status(500).send(ex.message);
    }
});



router.post('/generate-license-key', async (req, res) => {
    try {
        var mykey = crypto.createCipher('aes-128-cbc', '4bmsrr65ah3oas5d4sd54st11k');
        var licenseData = req.body;
        //macAddress examples => 00-09-0F-FE-00-01
        //clients?doors?cameras?zones?lprs?tags?fireDetectors?fingerprints?faceRecognition
        var mystr = mykey.update(`${licenseData.macAddress}?${licenseData.numOfClients}?${licenseData.numOfDoors}?${licenseData.numOfCameras}?${licenseData.numOfZones}?${licenseData.numOfLprs}?${licenseData.numOfTags}?${licenseData.numOfFireDetectors}?${licenseData.numOfFingerprints}?${licenseData.numOfFaceRecognitions}`, 'utf8', 'hex')
        mystr += mykey.final('hex');

        res.send({ licenseKey: mystr });
    }
    catch (ex) {
        log.error(ex.message);
        res.status(500).send(ex.message);
    }
});

function decodeLicense(licenseKey) {
    var mykey = crypto.createDecipher('aes-128-cbc', '4bmsrr65ah3oas5d4sd54st11k');
    var mystr = mykey.update(licenseKey, 'hex', 'utf8');
    mystr += mykey.final('utf8');
    var res = mystr.split("?");

    return {
        macAddress: res[0],
        clients: +res[1],
        doors: +res[2],
        cameras: +res[3],
        zones: +res[4],
        lprs: +res[5],
        tags: +res[6],
        fireDetectors: +res[7],
        fingerPrints: +res[8],
        faceRecognitions: +res[9]
    };
}

module.exports = router;