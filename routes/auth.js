const express = require('express');
const db = require('../db');
const log = require('../global-functions/console');
const router = express.Router();

const SCHEMA = 'subscription_management';

router.post('/get-corporations', async (req, res) => {
    try {
        var query = `SELECT * FROM ${SCHEMA}.corporation; 
        SELECT * FROM ${SCHEMA}.corporation_website;`;

        var response = await db.query(query);

        res.send(response);
    }
    catch (ex) {
        log.error(ex.message);
        res.status(500).send(ex.message);
    }
});


router.post('/login', async (req, res) => {
    try {
        var query = `SELECT * FROM ${SCHEMA}.users as u WHERE u.userName = '${req.body.userName}';`;

        var response = await db.query(query);

        if (response.length == 1 && response[0].password == req.body.password){
            response.map(i => delete i.password)
            res.status(200).send(response[0]);
        }

        else
            res.status(200).send(false);
    }
    catch (ex) {
        log.error(ex.message);
        res.status(500).send(ex.message);
    }
});

module.exports = router;