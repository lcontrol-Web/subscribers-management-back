const express = require('express');
const db = require('../db');
const log = require('../global-functions/console');
const router = express.Router();
const dbLogs = require('../global-functions/db-logs');

const SCHEMA = 'subscription_management';

router.post('/get-users', async (req, res) => {
    try {
        var query = `SELECT userId, userName, fullName, email, phone, allowedToCreateUser, allowedToCreateSubscriber, allowedToUpdateSubscriber, allowedToGenerateLicense        
                     FROM ${SCHEMA}.users; `;

        var response = await db.query(query);

        res.send(response);
    }
    catch (ex) {
        log.error(ex.message);
        res.status(500).send(ex.message);
    }
});


router.post('/add-user', async (req, res) => {
    try {
        var user = req.body;
        var query = `INSERT INTO ${SCHEMA}.users (userName, password, fullName, email, phone, allowedToCreateUser, allowedToCreateSubscriber, allowedToUpdateSubscriber, allowedToGenerateLicense)
                     VALUES ('${user.userName}',
                     '${user.password}',
                     '${user.fullName}', 
                     ${user.email ? "'" + user.email + "'" : 'null'}, 
                     ${user.phone ? "'" + user.phone + "'" : 'null'},
                     ${user.allowedToCreateUser},
                     ${user.allowedToCreateSubscriber},
                     ${user.allowedToUpdateSubscriber},
                     ${user.allowedToGenerateLicense});
                     
                    SELECT userId, userName, fullName, email, phone, allowedToCreateUser, allowedToCreateSubscriber, allowedToUpdateSubscriber, allowedToGenerateLicense 
                    FROM ${SCHEMA}.users
                    WHERE ${SCHEMA}.users.userId = LAST_INSERT_ID();`;

        var response = await db.query(query);

        res.send(response[1][0]);

        dbLogs.logAction(`Add Operator "${user.userName}"`, `Operator "${user.userName}" was added`, user.operatorId);
    }
    catch (ex) {
        log.error(ex.message);
        res.status(500).send(ex.message);
    }
});

router.post('/edit-user', async (req, res) => {
    try {
        var user = req.body;

        var query = `SELECT * FROM ${SCHEMA}.users WHERE ${SCHEMA}.users.userId = ${user.userId};`;
        var response = await db.query(query);
        var oldUser = response[0];
        var changes = '';

        for (let field of Object.keys(oldUser)) {
            if (oldUser[field] != user[field] && field !== 'password')
                changes += `${field}: "${oldUser[field]}" => "${user[field]}";`;
            else if (field == 'password' && user.password)
                changes += `password was changed; `;
        }

        query = `
            UPDATE ${SCHEMA}.users SET
                userName = '${user.userName}',
                ${(user.password ? "password = '" + user.password + "'," : '')}
                fullName = '${user.fullName}', 
                email = ${user.email ? "'" + user.email + "'" : 'null'},
                phone = ${user.phone ? "'" + user.phone + "'" : 'null'},
                allowedToCreateUser = ${user.allowedToCreateUser},
                allowedToCreateSubscriber = ${user.allowedToCreateSubscriber},
                allowedToUpdateSubscriber = ${user.allowedToUpdateSubscriber}, 
                allowedToGenerateLicense = ${user.allowedToGenerateLicense}
            WHERE ${SCHEMA}.users.userId = ${user.userId};
                     
            SELECT userId, userName, fullName, email, phone, allowedToCreateUser, allowedToCreateSubscriber, allowedToUpdateSubscriber, allowedToGenerateLicense 
            FROM ${SCHEMA}.users
            WHERE ${SCHEMA}.users.userId = ${user.userId};`;

        response = await db.query(query);

        res.send(response[1][0]);

        if (changes.length)
            dbLogs.logAction(`Update Operator "${user.userName}"`, `Operator "${oldUser.userName}" updated;` + changes, user.operatorId);
    }
    catch (ex) {
        log.error(ex.message);
        res.status(500).send(ex.message);
    }
});

router.post('/delete-user', async (req, res) => {
    try {

        var userId = req.body.userId;
        var query = `SELECT userName FROM ${SCHEMA}.users WHERE userId = ${userId};`;

        var response = await db.query(query);
        var userName = response[0].userName;

        query = `DELETE FROM ${SCHEMA}.users WHERE userId = ${userId};`;

        response = await db.query(query);

        res.send(response);

        dbLogs.logAction(`Delete Operator "${userName}"`, `Operator "${userName}" was deleted`, req.body.operatorId);
    }
    catch (ex) {
        log.error(ex.message);
        res.status(500).send(ex.message);
    }
});

module.exports = router;