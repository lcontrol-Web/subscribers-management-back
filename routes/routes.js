const express = require('express');
const auth = require('./auth');
const users = require('./users');
const subscribers = require('./subscribers');
const actionLogs = require('./action-logs');

// for error handling
const error = require('../middleware/error');

module.exports = function (app) {
    app.use(express.json());
    app.use('/auth', auth);
    app.use('/users', users);
    app.use('/subscribers', subscribers);
    app.use('/action-logs', actionLogs);

    // this middleware error function will activate after the routes above, by calling the next() function in the routes
    app.use(error);
};