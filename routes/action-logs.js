const express = require('express');
const db = require('../db');
const log = require('../global-functions/console');
const router = express.Router();

const LOGS_TABLE = 'subscription_management.action_logs';
const USERS_TABLE = 'subscription_management.users';

router.post('/get-logs', async (req, res) => {
    try {
        var query = `
            SELECT t.*, u.userName 
            FROM ${LOGS_TABLE} as t, ${USERS_TABLE} as u
            WHERE t.userId = u.userId
            ORDER BY t.date DESC;`;

        var response = await db.query(query);

        res.status(200).send(response);
    }
    catch (ex) {
        log.error(ex.message);
        res.status(500).send(ex.message);
    }
});

module.exports = router;