const settings = require('./settings.json');
const mysql = require('mysql');
const log = require('./global-functions/console');

var pool = mysql.createPool({
    connectionLimit: 5,
    host: settings.db.host,
    port: settings.db.port,
    user: settings.db.user,
    password: settings.db.password,
    multipleStatements: true
});

var activeConneciton = false;
var initRun = true;
var initRun2 = true;

function query(q) {
    return new Promise((resolve, reject) => {

        pool.getConnection((err, connection) => {
            // error in conneciton
            if (err) {
                reject(err);

                if (initRun2 || activeConneciton){
                    initRun2 = false;
                    log.error('Lost connection with database');
                }

                activeConneciton = false;
                return;
            }

            if (!activeConneciton) {
                log.msg('Database reconnected!');
                onReconnect();
                activeConneciton = true;
            }

            connection.query(q, (err, rows) => {
                // error in query
                if (err) {
                    reject(err);
                }
                else
                    resolve(rows);

                connection.release();
            });
        });
    });
}

function onReconnect() {
    if (initRun) {
        initRun = false;
    }
}

query('SELECT 1;');

module.exports.query = query;
