const db = require('../db');
const dateFormat = require('./date');

async function logAction(action, description, userId) {
    var query = `
        INSERT INTO subscription_management.action_logs (userId, action, description, date)
        VALUES (${userId}, '${action}', '${description}', '${dateFormat(new Date(), 'Date', 'yyyy-MM-dd HH:mm:ss')}');
    `;

    var response = await db.query(query);
};

module.exports.logAction = logAction;