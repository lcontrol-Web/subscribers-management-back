const zoneStatusNameList = ['open', 'armed', 'ready', 'alarm', 'bypass', 'home', 'duress', 'falseCode', 'fire', 'panic', 'medic', 'exitOpenZone', 'exists', 'resetRequired', 'noActivityAlert', 'groupAArm', 'groupBArm', 'groupCArm', 'groupDArm'];
const partitionStatusNameList = ['armed', 'ready', 'alarm', 'home', 'duress', 'falseCode', 'fire', 'panic', 'medic', 'exitOpenZone', 'exists', 'resetRequired', 'noActivityAlert', 'groupAArm', 'groupBArm', 'groupCArm', 'groupDArm'];


function sleep(duration) {
    return new Promise(resolve => {
        setTimeout(() => resolve(), duration);
    })
}

module.exports.zoneStatusNameList = zoneStatusNameList;
module.exports.partitionStatusNameList = partitionStatusNameList;
module.exports.sleep = sleep;